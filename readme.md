# mdast-flatten-image-paragraphs

Transforms a markdown AST ([MDAST](https://github.com/syntax-tree/mdast)) so that image nodes are **not** inside paragraph nodes.

## Installation

```sh
npm install mdast-flatten-image-paragraphs
```

## Usage

```js
// ...
var flattenImageParagraphs = require('mdast-flatten-image-paragraphs');

unified()
  .use(parse)
  .use(flattenImageParagraphs)
// ...
```

Markdown document:

```
# Title

Some text here.

![my image](./foo.png)

More text.
```

Input AST:

```
root[4] (1:1-9:1, 0-62)
├─ heading[1] (2:1-2:8, 1-8) [depth=1]
│  └─ text: "Title" (2:3-2:8, 3-8)
├─ paragraph[1] (4:1-4:16, 10-25)
│  └─ text: "Some text here." (4:1-4:16, 10-25)
├─ paragraph[1] (6:1-6:23, 27-49)
│  └─ image (6:1-6:23, 27-49) [url="./foo.png"][alt="my image"]
└─ paragraph[1] (8:1-8:11, 51-61)
   └─ text: "More text." (8:1-8:11, 51-61)
```

Output AST:

```
root[4] (1:1-9:1, 0-62)
├─ heading[1] (2:1-2:8, 1-8) [depth=1]
│  └─ text: "Title" (2:3-2:8, 3-8)
├─ paragraph[1] (4:1-4:16, 10-25)
│  └─ text: "Some text here." (4:1-4:16, 10-25)
├─ image (6:1-6:23, 27-49) [url="./foo.png"][alt="my image"]
└─ paragraph[1] (8:1-8:11, 51-61)
   └─ text: "More text." (8:1-8:11, 51-61)
```

## License

[MIT](LICENSE)