const flatMap = require('unist-util-flatmap');

function flattenImageParagraphs() {
  return ast => {
    return flatMap(ast, node => {
      if (
        node.type === 'paragraph' &&
        node.children.some(child => child.type === 'image')
      ) {
        return node.children.map(child => {
          if (child.type === 'image') return child;
          return {
            type: 'paragraph',
            children: [child],
            position: child.position,
          };
        });
      }
      return [node];
    });
  };
}

module.exports = flattenImageParagraphs;
