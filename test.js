const test = require('tape');
const unified = require('unified');
const remarkParse = require('remark-parse');
const inspect = require('unist-util-inspect');
const flattenImageParagraphs = require('./index');

test('it converts "paragraph > image" to "image"', t => {
  t.plan(2);

  const markdown = `
# Title

Some text here.

![my image](./foo.png)

More text.
`;

  const actualInput = unified()
    .use(remarkParse, {commonmark: true})
    .parse(markdown);

  console.log(inspect(actualInput));

  const expectedInput = {
    type: 'root',
    children: [
      {
        type: 'heading',
        depth: 1,
        children: [
          {
            type: 'text',
            value: 'Title',
            position: {
              start: {line: 2, column: 3, offset: 3},
              end: {line: 2, column: 8, offset: 8},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 2, column: 1, offset: 1},
          end: {line: 2, column: 8, offset: 8},
          indent: [],
        },
      },
      {
        type: 'paragraph',
        children: [
          {
            type: 'text',
            value: 'Some text here.',
            position: {
              start: {line: 4, column: 1, offset: 10},
              end: {line: 4, column: 16, offset: 25},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 4, column: 1, offset: 10},
          end: {line: 4, column: 16, offset: 25},
          indent: [],
        },
      },
      {
        type: 'paragraph',
        children: [
          {
            type: 'image',
            title: null,
            url: './foo.png',
            alt: 'my image',
            position: {
              start: {line: 6, column: 1, offset: 27},
              end: {line: 6, column: 23, offset: 49},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 6, column: 1, offset: 27},
          end: {line: 6, column: 23, offset: 49},
          indent: [],
        },
      },
      {
        type: 'paragraph',
        children: [
          {
            type: 'text',
            value: 'More text.',
            position: {
              start: {line: 8, column: 1, offset: 51},
              end: {line: 8, column: 11, offset: 61},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 8, column: 1, offset: 51},
          end: {line: 8, column: 11, offset: 61},
          indent: [],
        },
      },
    ],
    position: {
      start: {line: 1, column: 1, offset: 0},
      end: {line: 9, column: 1, offset: 62},
    },
  };
  t.deepEquals(actualInput, expectedInput, 'input looks good');

  const actualOutput = flattenImageParagraphs()(actualInput);

  console.log(inspect(actualOutput));

  const expectedOutput = {
    type: 'root',
    children: [
      {
        type: 'heading',
        depth: 1,
        children: [
          {
            type: 'text',
            value: 'Title',
            position: {
              start: {line: 2, column: 3, offset: 3},
              end: {line: 2, column: 8, offset: 8},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 2, column: 1, offset: 1},
          end: {line: 2, column: 8, offset: 8},
          indent: [],
        },
      },
      {
        type: 'paragraph',
        children: [
          {
            type: 'text',
            value: 'Some text here.',
            position: {
              start: {line: 4, column: 1, offset: 10},
              end: {line: 4, column: 16, offset: 25},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 4, column: 1, offset: 10},
          end: {line: 4, column: 16, offset: 25},
          indent: [],
        },
      },
      {
        type: 'image',
        title: null,
        url: './foo.png',
        alt: 'my image',
        position: {
          start: {line: 6, column: 1, offset: 27},
          end: {line: 6, column: 23, offset: 49},
          indent: [],
        },
      },
      {
        type: 'paragraph',
        children: [
          {
            type: 'text',
            value: 'More text.',
            position: {
              start: {line: 8, column: 1, offset: 51},
              end: {line: 8, column: 11, offset: 61},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 8, column: 1, offset: 51},
          end: {line: 8, column: 11, offset: 61},
          indent: [],
        },
      },
    ],
    position: {
      start: {line: 1, column: 1, offset: 0},
      end: {line: 9, column: 1, offset: 62},
    },
  };

  t.deepEquals(actualOutput, expectedOutput, 'output looks good');
});
